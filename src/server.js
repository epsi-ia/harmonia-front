const open = require('amqplib').connect('amqp://rabbitmq');
const Path = require('path');
const Hapi = require('hapi');
const MidiConvert = require('MidiConvert');

const server = new Hapi.Server();
server.connection({ port: 8080, host: '0.0.0.0' });
server.register([require('inert'), require('susie')], function (err) {

    server.route({
        method: 'GET',
        path: '/sse',
        handler: function (request, reply) {
            // Consumer 
            open.then(function(conn) {
              return conn.createChannel();
            }).then(function(ch) {
              return ch.assertQueue("music").then(function(ok) {
                console.log(ok)
                return ch.consume("music", function(msg) {
                  if (msg !== null) {
                    const partLength = 512;
                    let nbParts = Math.ceil(msg.content.length / partLength);
                    let endParts = msg.content.length % partLength;
                    console.log(msg.content.length, nbParts, endParts);
                    for(let i = 0; i < nbParts; i++){
                      console.log(i);
                      let buf = msg.content.slice(i*partLength, (i+1 === nbParts)? i*partLength + endParts : partLength*(i+1));
                      console.log(i, buf);
                      reply.event({data: {totalPart : nbParts, current: i+1, data: buf.toString('base64')}})
                    }
                    ch.ack(msg);
                  }
                });
              });
            }).catch(console.warn);
        }
    });

    server.route({
      method: 'GET',
      path: '/play',
      handler : function(request, reply){
        open.then(function(conn) {
          return conn.createChannel();
        }).then(function(ch) {
          return ch.assertQueue("play").then(function(ok) {
            return ch.sendToQueue("play", new Buffer('play'));
          });
        }).catch(console.warn);
      }
    })

    server.route({
      method: 'GET',
      path: '/{pages*}',
      handler : {
        directory: {
          path: Path.join(__dirname, 'static'),
          listing: true
        }
      }
    })



    server.start((err) => {

        if (err) {
            throw err;
        }
        console.log(`Server running at: ${server.info.uri}`);
    });
});
