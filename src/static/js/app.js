let pianoDiv = document.getElementById('piano');
for(let i = 0; i < 128; i++){
  let div = document.createElement('div');
  div.id = i;
  div.className = "piano-touch"
  pianoDiv.appendChild(div);
}

document.getElementById('play').addEventListener('click', e => fetch('/play'))

const synth = new Tone.PolySynth(8).toMaster()
const colors = ['#1E90FF', '#9DBF15']
  const source = new EventSource('/sse');
  let fullMessage = "";
  let endTime = 0;
  source.onmessage = function(event){
    console.log(event);
    let data = JSON.parse(event.data);
    if(data.totalPart && data.current && data.current <= data.totalPart){
      fullMessage = `${fullMessage}${atob(data.data)}`
      if(data.current === data.totalPart) {
        let midi = MidiConvert.parse(fullMessage);
        console.log(midi)
        let tracks = midi.tracks.filter( track => track.channelNumber >= 0)
        Tone.Transport.bpm.value = midi.header.bpm;
        console.log(tracks)
        tracks.map((track, index) => {
            return new Tone.Part(function(time, note) {
                synth.triggerAttackRelease(note.name, note.duration, time, note.velocity)

                Tone.Draw.schedule(function(){
                  //the callback synced to the animation frame at the given time
                  document.getElementById(note.midi).style = `background: ${colors[index]}`
                  setTimeout(() => {
                    document.getElementById(note.midi).style =  ""
                  }, note.duration * 1000)
                }, time);
              }, track.notes).start(endTime)
        });
        endTime += midi.duration;
        Tone.Transport.start()
        Tone.Transport.on("stopped", () => endTime = 0)
      }
    }  else {
    }
    /*let midi = 
    */
    
  };

  source.addEventListener('end', function(){
    console.log(arguments);
    this.close();
  })

  source.onerror = console.warn
  
// }
